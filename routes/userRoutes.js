const express = require("express");
const router = express.Router();
const UserController = require("../controllers/userControllers")
const auth = require("../auth");


router.post("/register", (req, res) => {
	UserController.registerUser(req.body).then(result => res.send(result));
});


router.post("/login", (req, res) => {
	UserController.loginUser(req.body).then(result => res.send(result));
});


router.put("/usertype/:userId",auth.verify, (req,res) => {
	const data = {
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
		UserController.updateUserType(req.params.userId,req.body).then(result => res.send(result));
	} else {
		res.send(false)
	}
} )



module.exports = router;