const User = require("../models/User");
const bcrypt = require('bcrypt');
const auth = require("../auth")

module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		username: reqBody.username,
		mobileNo: reqBody.mobileNo,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) => {	
		if(error) {
			return false;
		} else {
			return true
		}
	})
}

module.exports.loginUser = (reqBody) => {
	return User.findOne({ username: reqBody.username }).then(result => {
		if(result == null) {
			return false;
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			
			if(isPasswordCorrect){
				return { accessToken : auth.createAccessToken(result.toObject())}
			} else {
				return false;
			}
		};
	});
};

module.exports.updateUserType = (userId, reqBody) => {
	let updatedUserType = {
		isAdmin: reqBody.isAdmin
	};

	return User.findByIdAndUpdate(userId, updatedUserType).then((course, error) => {
		if(error) {
			return false;
		} else {
			return true;
		}
	})
}